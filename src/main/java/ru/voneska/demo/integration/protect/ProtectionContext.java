package ru.voneska.demo.integration.protect;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.rits.cloning.Cloner;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Wither;
import ru.voneska.demo.integration.routes.AbstractPipeline;
import ru.voneska.demo.integration.routes.StepBasic;

import java.io.Serializable;

/**
 * @author Ivan Aksenov
 * @since
 */
@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ProtectionContext extends AbstractPipeline implements Serializable {

	private static Cloner cloner = new Cloner();

	@Wither
	@JsonProperty(value = "current-step")
	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
	@JsonIdentityReference(alwaysAsId = true)
	private StepBasic currentStep;

	@Wither
	private ExecutionContext execution;

	private boolean broken;

	public ProtectionContext duplicate() {
		return cloner.deepClone(this);
	}

}


