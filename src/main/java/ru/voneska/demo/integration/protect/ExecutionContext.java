package ru.voneska.demo.integration.protect;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Wither;

import java.io.Serializable;

/**
 * @author Ivan Aksenov
 * @since
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class ExecutionContext implements Serializable {

	private String operationId;

	private String documentId;

	private int[] pages;

	@Wither private int page;

	private String result;


}


