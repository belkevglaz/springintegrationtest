package ru.voneska.demo.integration.config;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Declarable;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.integration.amqp.dsl.Amqp;
import org.springframework.integration.amqp.outbound.AmqpOutboundEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.Transformers;
import org.springframework.integration.handler.LoggingHandler;
import org.springframework.integration.http.config.EnableIntegrationGraphController;
import org.springframework.integration.router.ExpressionEvaluatingRouter;
import org.springframework.messaging.MessageChannel;
import ru.voneska.demo.integration.actuators.AwesomeAggregator;
import ru.voneska.demo.integration.protect.ExecutionContext;
import ru.voneska.demo.integration.protect.ProtectionContext;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;

/**
 * @author Ivan Aksenov
 * @since
 */
@Slf4j
@EnableRabbit
@Configuration
@EnableIntegration
@EnableIntegrationGraphController
public class IntegrationFlowsConfiguration {

	public static final String START_QUEUE = "test.start.queue";
	public static final String ROUTE_QUEUE = "test.route.queue";
	public static final String SPLIT_QUEUE = "test.split.queue";
	public static final String CHECK_QUEUE = "test.check.queue";
	public static final String AGGR_QUEUE_TEMP = "test.aggr.queue";
	public static final String RESULT_QUEUE = "test.result.queue";
	public static final String NOTIFY_QUEUE = "test.notify.queue";

	public static final String PROTECT_AGGREGATE_EXCHANGE = "protection.aggr.exchange";

	private final Environment environment;

	@Autowired
	public IntegrationFlowsConfiguration(Environment environment) {
		this.environment = environment;
	}

	@Bean
	public AmqpAdmin amqpAdmin() {
		return new RabbitAdmin(connectionFactory());
	}

	@Bean
	public AmqpTemplate amqpTemplate() {
		final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory());
		rabbitTemplate.setMessageConverter(producerJackson2MessageConverter());
		return rabbitTemplate;
	}

	@Bean
	public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
		return new Jackson2JsonMessageConverter();
	}

	/**
	 * Создание фабрики подключений по AMQP протоколу к RabbitMQ
	 *
	 * @return {@link ConnectionFactory}
	 */
	@Bean
	public ConnectionFactory connectionFactory() {
		CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
		connectionFactory.setConnectionTimeout(3000);
		connectionFactory.setRequestedHeartBeat(30);
		return connectionFactory;
	}

	@Bean
	public String dynamicAggregateQueueName() {
		try {
			return AGGR_QUEUE_TEMP + "." + InetAddress.getLocalHost().getCanonicalHostName() + "." + environment.getProperty("server.port");
		} catch (UnknownHostException e) {
			log.error("Unable to get local IP address");
		}
		return "";
	}

	/**
	 * Создание необходимых артефактов на стороне RabbitMQ
	 *
	 * @return список {@link Declarable}
	 */
	@Bean
	public List<Declarable> configure() {

		Queue dlx = QueueBuilder.durable("DQL").
				withArgument("x-queue-mode", "lazy").build();

		Queue startQueue = QueueBuilder.durable(START_QUEUE)
				.withArgument("x-dead-letter-exchange", "")
				.withArgument("x-dead-letter-routing-key", "DQL").build();
		Queue routeQueue = QueueBuilder.durable(ROUTE_QUEUE)
				.withArgument("x-dead-letter-exchange", "")
				.withArgument("x-dead-letter-routing-key", "DQL").build();
		Queue splitQueue = QueueBuilder.durable(SPLIT_QUEUE)
				.withArgument("x-dead-letter-exchange", "")
				.withArgument("x-dead-letter-routing-key", "DQL").build();
		Queue checkQueue = QueueBuilder.durable(CHECK_QUEUE)
				.withArgument("x-dead-letter-exchange", "")
				.withArgument("x-dead-letter-routing-key", "DQL").build();
		Queue aggrQueue = QueueBuilder.durable(dynamicAggregateQueueName())
				.withArgument("x-dead-letter-exchange", "")
				.withArgument("x-dead-letter-routing-key", "DQL").build();
		Queue resultQueue = QueueBuilder.durable(RESULT_QUEUE)
				.withArgument("x-dead-letter-exchange", "")
				.withArgument("x-dead-letter-routing-key", "DQL").build();
		Queue notifyQueue = QueueBuilder.durable(NOTIFY_QUEUE)
				.withArgument("x-dead-letter-exchange", "")
				.withArgument("x-dead-letter-routing-key", "DQL").build();

		DirectExchange exchange = new DirectExchange("test.exchange", true, false);
		FanoutExchange aggregateExchange = new FanoutExchange(PROTECT_AGGREGATE_EXCHANGE, true, false);


		return Arrays.asList(
				dlx,
				startQueue,
				routeQueue,
				splitQueue,
				checkQueue,
				aggrQueue,
				resultQueue,
				notifyQueue,
				exchange,
				aggregateExchange,
				BindingBuilder.bind(aggrQueue).to(aggregateExchange)
		);
	}

	/* Starter configuration */

	@Bean
	public IntegrationFlow startFlow(AmqpTemplate amqpTemplate) {
		return IntegrationFlows
				.from(Amqp.inboundAdapter(connectionFactory(), START_QUEUE))
				.transform(Transformers.fromJson(ExecutionContext.class))
				.handle("awesomeStarter", "handle")
				.handle(Amqp.outboundAdapter(amqpTemplate).routingKey(ROUTE_QUEUE))
				.get();

	}

	/* Router configuration */

	@Bean
	public IntegrationFlow routeFlow() {

		val router = new ExpressionEvaluatingRouter("payload.currentStep.channel");
		router.setDefaultOutputChannelName("errorChannel");

		return IntegrationFlows
				.from(Amqp.inboundAdapter(connectionFactory(), ROUTE_QUEUE))
				.transform(Transformers.fromJson(ProtectionContext.class))
				.split("awesomeRouter", "splitByChannels")
				.route(router)
				.get();
	}

	/* Splitter configuration */

	@Bean
	public MessageChannel amqpChannelToSplitter() {
		return new DirectChannel();
	}

	@Bean
	@ServiceActivator(inputChannel = "amqpChannelToSplitter")
	public AmqpOutboundEndpoint amqpOutbound(AmqpTemplate amqpTemplate) {
		AmqpOutboundEndpoint outbound = new AmqpOutboundEndpoint(amqpTemplate);
		outbound.setRoutingKey(SPLIT_QUEUE);
		return outbound;
	}

	@Bean
	public IntegrationFlow splitStep(AmqpTemplate amqpTemplate) {
		return IntegrationFlows
				.from(Amqp.inboundAdapter(connectionFactory(), SPLIT_QUEUE))
				.transform(Transformers.fromJson(ProtectionContext.class))
				.split("awesomeSplitter", "split")
				.handle(Amqp.outboundAdapter(amqpTemplate).routingKey(ROUTE_QUEUE))
				.get();
	}

	/* Notification configuration */

	@Bean
	public MessageChannel amqpChannelToNotify() {
		return new DirectChannel();
	}

	@Bean
	@ServiceActivator(inputChannel = "amqpChannelToNotify")
	public AmqpOutboundEndpoint amqpOutboundNotify(AmqpTemplate amqpTemplate) {
		AmqpOutboundEndpoint outbound = new AmqpOutboundEndpoint(amqpTemplate);
		outbound.setRoutingKey(NOTIFY_QUEUE);
		return outbound;
	}

	/* Checker configuration */

	@Bean
	public MessageChannel amqpChannelToChecker() {
		return new DirectChannel();
	}

	@Bean
	@ServiceActivator(inputChannel = "amqpChannelToChecker")
	public AmqpOutboundEndpoint amqpOutboundCheck(AmqpTemplate amqpTemplate) {
		AmqpOutboundEndpoint outbound = new AmqpOutboundEndpoint(amqpTemplate);
		outbound.setRoutingKey(CHECK_QUEUE);
		return outbound;
	}


	@Bean
	public IntegrationFlow checkStep(AmqpTemplate amqpTemplate) {
		return IntegrationFlows
				.from(Amqp.inboundAdapter(connectionFactory(), CHECK_QUEUE))
				.transform(Transformers.fromJson(ProtectionContext.class))
				.handle("awesomeChecker", "check")
				.handle(Amqp.outboundAdapter(amqpTemplate).routingKey(ROUTE_QUEUE))
				.get();
	}

	/* Aggregator Configuration */

	@Bean
	public MessageChannel amqpChannelToAggregator() {
		return new DirectChannel();
	}

	@Bean
	@ServiceActivator(inputChannel = "amqpChannelToAggregator")
	public AmqpOutboundEndpoint amqpOutboundAggregate(AmqpTemplate amqpTemplate) {
		AmqpOutboundEndpoint outbound = new AmqpOutboundEndpoint(amqpTemplate);
		outbound.setExchangeName(PROTECT_AGGREGATE_EXCHANGE);
		return outbound;
	}


	@Bean
	public AwesomeAggregator getAggregator() {
		return new AwesomeAggregator();
	}

	@Bean
	public IntegrationFlow aggregateStep(AmqpTemplate amqpTemplate) {
		return IntegrationFlows
				.from(Amqp.inboundAdapter(connectionFactory(), dynamicAggregateQueueName()))
				.transform(Transformers.fromJson(ProtectionContext.class))
				.aggregate(a -> a.processor(getAggregator()))
				.handle(Amqp.outboundAdapter(amqpTemplate).routingKey(ROUTE_QUEUE))
				.get();
	}

	/* Result (Finisher) Configuration */

	@Bean
	public MessageChannel amqpChannelToResult() {
		return new DirectChannel();
	}

	@Bean
	@ServiceActivator(inputChannel = "amqpChannelToResult")
	public AmqpOutboundEndpoint amqpOutboundResult(AmqpTemplate amqpTemplate) {
		AmqpOutboundEndpoint outbound = new AmqpOutboundEndpoint(amqpTemplate);
		outbound.setRoutingKey(RESULT_QUEUE);
		return outbound;
	}

	@Bean
	public IntegrationFlow finishStep(AmqpTemplate amqpTemplate) {
		return IntegrationFlows
				.from(Amqp.inboundAdapter(connectionFactory(), RESULT_QUEUE))
				.transform(Transformers.fromJson(ProtectionContext.class))
				.handle("awesomeChecker", "finish")
				.handle(Amqp.outboundAdapter(amqpTemplate).routingKey(ROUTE_QUEUE))
				.get();
	}

	/* Error Handler (DEAD-LETTER-QUEUE) Configuration */

	@Bean
	public IntegrationFlow errorHandle(AmqpTemplate amqpTemplate) {
		return IntegrationFlows
				.from("errorChannel")
				.log(LoggingHandler.Level.ERROR)
				.handle(Amqp.outboundAdapter(amqpTemplate).routingKey("DQL"))
				.get();
	}


}
