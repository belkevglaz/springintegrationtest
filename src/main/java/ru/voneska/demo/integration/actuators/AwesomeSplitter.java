package ru.voneska.demo.integration.actuators;

import lombok.val;
import org.springframework.integration.annotation.Splitter;
import org.springframework.stereotype.Component;
import ru.voneska.demo.integration.protect.ProtectionContext;
import ru.voneska.demo.integration.routes.StepStates;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author Ivan Aksenov
 * @since
 */
@Component
public class AwesomeSplitter {

	@Splitter
	public List<ProtectionContext> split(ProtectionContext pipeline) {
		System.out.println("AwesomeSplitter.split");
		pipeline.getCurrentStep().done();

		List<ProtectionContext> pp = Arrays.stream(pipeline.getExecution().getPages()).boxed().map(
				value ->  {
					val context = pipeline.duplicate();
					context.setExecution(pipeline.getExecution().withPage(value));
					return context;
				}
		).collect(Collectors.toList());
		return pp;

	}



}
