package ru.voneska.demo.integration.actuators;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import ru.voneska.demo.integration.protect.ExecutionContext;
import ru.voneska.demo.integration.protect.ProtectionContext;

import java.io.IOException;
import java.util.UUID;

/**
 * @author Ivan Aksenov
 * @since
 */
@Slf4j
@Component
public class AwesomeStarter {

	private ObjectMapper objectMapper;

	@Value("classpath:/routes.json")
	Resource resourceFile;

	@Autowired
	public AwesomeStarter(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}

	public ProtectionContext handle(Message<ExecutionContext> message) throws IOException {
		System.out.println("AwesomeStarter.handle");
		try {
			ProtectionContext pipeline = getPipeline("");

			//prepare execution context for next step
			message.getPayload().setDocumentId(UUID.randomUUID().toString());
			pipeline.setExecution(message.getPayload());
			return pipeline;
		} catch (IOException e) {
			log.error("Unable to get route context");
			throw e;
		}
	}

	private ProtectionContext getPipeline(String mimeType) throws IOException {
		return objectMapper.readValue(resourceFile.getFile(), ProtectionContext.class);
	}

}
