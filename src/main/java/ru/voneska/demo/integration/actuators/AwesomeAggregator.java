package ru.voneska.demo.integration.actuators;

import lombok.val;
import org.springframework.integration.annotation.Aggregator;
import org.springframework.integration.annotation.CorrelationStrategy;
import org.springframework.integration.annotation.ReleaseStrategy;
import org.springframework.stereotype.Component;
import ru.voneska.demo.integration.protect.ProtectionContext;

import java.util.List;

/**
 * @author Ivan Aksenov
 * @since
 */
@Component
public class AwesomeAggregator {


	@CorrelationStrategy
	public String correlate(ProtectionContext context) {
		System.out.println("AwesomeAggregator.correlate");
		return context.getExecution().getDocumentId();
	}

	@ReleaseStrategy
	public boolean release(List<ProtectionContext> messages) {
		System.out.println("AwesomeAggregator.release");

		if (messages.size() > 0) {
			return messages.get(0).getExecution().getPages().length == messages.size();
		} else {
			return false;
		}
	}

	@Aggregator
	public ProtectionContext aggregate(List<ProtectionContext> messages) {
		System.out.println("AwesomeAggregator.aggregate");

		int sum = messages.stream().mapToInt(m -> m.getExecution().getPage()).sum();
		val context = messages.get(0).duplicate();
		context.getExecution().setResult(String.valueOf(sum));
		context.getCurrentStep().done();

		boolean broken = messages.stream().anyMatch(ProtectionContext::isBroken);

		context.setBroken(broken);

		return context;
	}
}
