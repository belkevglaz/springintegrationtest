package ru.voneska.demo.integration.actuators;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.voneska.demo.integration.protect.ProtectionContext;

/**
 * @author Ivan Aksenov
 * @since
 */
@Slf4j
@Component
public class AwesomeChecker {

	public ProtectionContext check(ProtectionContext context) {
		System.out.println("AwesomeChecker.check : " + "processing page " + context.getExecution().getPage());
		context.getExecution().setOperationId("44444444444");
		context.getCurrentStep().done();

		// emulate exception
		if (context.getExecution().getPage() == 1000) {
			try {
				throw new Exception("Page 1000. Genarated Exception");
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				// set this page is exception and shoudl be context is broken;
				context.getCurrentStep().error(e);
				context.setBroken(true);
			}
		}
		return context;
	}

	public ProtectionContext finish(ProtectionContext context) {
		System.out.println("AwesomeChecker.finish");
		log.info("Sum of page numbers is = [{}] and is broken [{}]", context.getExecution().getResult(), context.isBroken());
		context.getCurrentStep().done();
		return context;
	}

}
