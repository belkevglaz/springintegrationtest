package ru.voneska.demo.integration.actuators;

import lombok.val;
import org.springframework.integration.annotation.Splitter;
import org.springframework.stereotype.Component;
import ru.voneska.demo.integration.protect.ProtectionContext;
import ru.voneska.demo.integration.routes.PipelineUtils;
import ru.voneska.demo.integration.routes.StepBasic;
import ru.voneska.demo.integration.routes.StepStates;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Ivan Aksenov
 * @since
 */
@Component
public class AwesomeRouter {


	@Splitter
	public List<ProtectionContext> splitByChannels(ProtectionContext context) {
		System.out.println("AwesomeRouter.splitByChannels");

		if (context.getCurrentStep() == null) {

			// if current null, that means we start processing and need get first step
			System.out.println("context.getCurrentStep() = " + context.getCurrentStep());
			context.setCurrentStep(PipelineUtils.getFirstStep(context));
			context.getCurrentStep().done();

		} else if (context.getCurrentStep().isError()) {

			// if current step done with error - send further.
			System.out.println("context.getCurrentStep().getId() = " + context.getCurrentStep().getId() + " is error");

		} else if (!context.getCurrentStep().isDone()) {

			// if current step not done (reverted back) - to error
			System.out.println("context.getCurrentStep().getId() = " + context.getCurrentStep().getId() + " not done");
			context.setCurrentStep(PipelineUtils.makeError());
			return Collections.singletonList(context);

		}

		System.out.println("context.getCurrentStep().getId() = " + context.getCurrentStep().getId() + " is done");

		// if current done - prepare next steps
		List<StepBasic> nextSteps = context.getCurrentStep().getNextSteps();

		if (nextSteps == null) {
			// if next step absent - send to nullChannel
			val toNull = context.duplicate();
			toNull.setCurrentStep(PipelineUtils.makeNullEmpty());
			return Collections.singletonList(toNull);
		} else {

			return nextSteps.stream().map(step -> {
				val c = context.duplicate();
				c.setCurrentStep(step);
				c.getCurrentStep().routed();
				return c;
			}).collect(Collectors.toList());
		}

	}

}
