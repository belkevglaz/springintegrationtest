package ru.voneska.demo.integration.routes;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Wither;

import java.util.List;

/**
 * @author Ivan Aksenov
 * @since
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StepBasic implements PipelineStep, Cloneable {

	private String id;

	@JsonProperty(value = "channel")
	private String channel;

	@JsonProperty(value = "config")
	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
	@JsonIdentityReference(alwaysAsId = true)
	private StepConfig config;

	private int state;

	@JsonProperty(value = "next-steps")
	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
	@JsonIdentityReference(alwaysAsId = true)
	private List<StepBasic> nextSteps;

	private String error;

	@Override
	public boolean isDone() {
		return state == StepStates.DONE;
	}

	public boolean isError() {
		return state == StepStates.ERROR;
	}

	public void done() {
		this.state = StepStates.DONE;
	}

	public void error() {
		this.state = StepStates.ERROR;
	}

	public void error(String e) {
		this.error();
		this.error = e;
	}
	public void error(Exception e) {
		this.error(e.getMessage());
	}

	public void routed() {
		this.state = StepStates.ROUTED;
	}
}
