package ru.voneska.demo.integration.routes;

import java.util.List;

/**
 * @author Ivan Aksenov
 * @since
 */
public interface Pipeline {

	String getName();

	String getType();

	<F extends PipelineStep> List<F> getSteps();

	List<StepConfig> getConfigs();

}
