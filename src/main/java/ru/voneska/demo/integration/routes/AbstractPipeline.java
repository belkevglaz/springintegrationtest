package ru.voneska.demo.integration.routes;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Ivan Aksenov
 * @since
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractPipeline implements Pipeline {

	private String name;

	private String type;

	private List<StepBasic> steps;

	private List<StepConfig> configs;

}
