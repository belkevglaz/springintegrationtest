package ru.voneska.demo.integration.routes;

/**
 * @author Ivan Aksenov
 * @since
 */
public final class PipelineUtils {

	public static <PS extends PipelineStep> PS getFirstStep(Pipeline pipeline) {
		//noinspection unchecked
		return (PS) pipeline.getSteps().stream().findFirst().orElseThrow(() -> new IllegalArgumentException("Unable start process due to absent first step"));
	}

	public static StepBasic makeNullEmpty() {
		return StepBasic.builder().channel("nullChannel").build();
	}

	public static StepBasic makeError() {
		return StepBasic.builder().channel("errorChannel").build();
	}
}
