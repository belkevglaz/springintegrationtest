package ru.voneska.demo.integration.routes;

import java.util.List;

/**
 * @author Ivan Aksenov
 * @since
 */
public interface PipelineStep {

	String getId();

	String getChannel();

	StepConfig getConfig();

	<S extends PipelineStep> List<S> getNextSteps();

	boolean isDone();

	boolean isError();
}
