package ru.voneska.demo.integration.routes;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * @author Ivan Aksenov
 * @since
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StepConfig  {

	private String id;

	private Map<String, Object> params;

}
