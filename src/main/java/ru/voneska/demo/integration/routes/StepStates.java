package ru.voneska.demo.integration.routes;

import lombok.Data;

/**
 * @author Ivan Aksenov
 * @since
 */
public interface StepStates {

	public static final int INCOMPLETE = 0;
	public static final int ROUTED = 1;
	public static final int DONE = 2;
	public static final int ERROR = -9999;

}
