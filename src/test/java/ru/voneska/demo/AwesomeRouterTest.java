package ru.voneska.demo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.context.junit4.SpringRunner;
import ru.voneska.demo.integration.actuators.AwesomeRouter;
import ru.voneska.demo.integration.protect.ProtectionContext;
import ru.voneska.demo.integration.routes.StepStates;

import java.io.IOException;
import java.util.List;

/**
 * @author Ivan Aksenov
 * @since
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Configuration
public class AwesomeRouterTest {

	@Autowired
	private AwesomeRouter router;

	@Autowired
	private ObjectMapper objectMapper;

	@Value("classpath:/routes.json")
	Resource resourceRoute;

	@Autowired
	private RouterGateway gateway;


	@Test
	public void testRouterFlowCurrentStepStartByGateway() throws IOException {
		ProtectionContext pipeline = objectMapper.readValue(resourceRoute.getFile(), ProtectionContext.class);
		gateway.sendMessage(convertToGatewayMessage(pipeline));

	}

	@Test
	public void testRouterFlowCurrentStepStartByMethod() throws IOException {
		ProtectionContext pipeline = objectMapper.readValue(resourceRoute.getFile(), ProtectionContext.class);

		List<ProtectionContext> steps = router.splitByChannels(pipeline);
		steps.forEach(this::printContextAsString);

	}

	@Test
	public void testRouterFlowSplitByChannelSecondByMethod() throws IOException {
		ProtectionContext pipeline = objectMapper.readValue(resourceRoute.getFile(), ProtectionContext.class);

		List<ProtectionContext> steps = router.splitByChannels(pipeline);
		// emulate that steps were completed
		steps.forEach(p -> p.getCurrentStep().setState(StepStates.DONE));

		steps.stream().flatMap(o -> router.splitByChannels(o).stream()).forEach(this::printContextAsString);

	}


	private Message convertToGatewayMessage(ProtectionContext context) {
		try {
			return MessageBuilder.withPayload(objectMapper.writeValueAsString(context).getBytes()).build();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void printContextAsString(ProtectionContext context) {
		try {
			System.out.println(objectMapper.writeValueAsString(context));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}

	private void prettyPrintContextAsString(ProtectionContext context) {
		try {
			System.out.println(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(context));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}

	@MessagingGateway(defaultRequestChannel = "routeFlow.channel#0")
	public interface RouterGateway {

		@Gateway
		void sendMessage(Message<?> message);
	}

}
