package ru.voneska.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringRunner;
import ru.voneska.demo.integration.protect.ExecutionContext;
import ru.voneska.demo.integration.protect.ProtectionContext;
import ru.voneska.demo.integration.routes.PipelineUtils;
import ru.voneska.demo.integration.routes.StepBasic;
import ru.voneska.demo.integration.routes.StepConfig;
import ru.voneska.demo.integration.routes.StepStates;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JsonRouteValidationTest {

	@Autowired
	private ObjectMapper objectMapper;

	@Value("classpath:/routes.json")
	Resource resourceRoute;

	@Value("classpath:/map.json")
	Resource resourceMap;

	/**
	 * Проверям что можем загрузить маршрут
	 *
	 * @throws IOException
	 */
	@Test
	public void testDeserializeRoute() throws IOException {
		ProtectionContext pipeline = objectMapper.readValue(resourceRoute.getFile(), ProtectionContext.class);
		System.out.println(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(pipeline));;

	}

	/**
	 * Проверям как десериализуется мап в конфигах
	 *
	 * @throws IOException
	 */
	@Test
	public void testSerializeConfigMap() throws IOException {
		StepConfig config = objectMapper.readValue(resourceMap.getFile(), StepConfig.class);
		System.out.println("config = " + config);
	}

	/**
	 * Проверяем что можем менять значение и генерировать несколько уникальных контекстов
	 *
	 * @throws IOException
	 */
	@Test
	public void testLombokWither() throws IOException {
		ExecutionContext exec = ExecutionContext.builder().documentId("1234").operationId("4321").pages(new int[] {1,2,3}).build();
		ProtectionContext pipeline = objectMapper.readValue(resourceRoute.getFile(), ProtectionContext.class);
		pipeline.setExecution(exec);

		System.out.println("pipeline = " + pipeline);

		List<ProtectionContext> contextList = Arrays.stream(exec.getPages()).boxed().map(
				value -> {
					val context = ProtectionContext.builder().build();
					context.setConfigs(pipeline.getConfigs());
					context.setSteps(pipeline.getSteps());
					context.setName(pipeline.getName());
					context.setType(pipeline.getType());
					context.setExecution(pipeline.getExecution().withPage(value));
					context.setCurrentStep(pipeline.getCurrentStep());
					return context;
				}
		).collect(Collectors.toList());

		System.out.println("contextList.size() = " + contextList.size());
		contextList.stream().forEach(System.out::println);

	}

	/**
	 * Проверяем что можем сериализовать и десериализовать объекты по ссылке в json-е
	 *
	 * @throws IOException
	 */
	@Test
	public void testSerializeByReferenceInJson() throws IOException {
		ProtectionContext pipeline = objectMapper.readValue(resourceRoute.getFile(), ProtectionContext.class);

		StepBasic first = PipelineUtils.getFirstStep(pipeline);
		first.setState(StepStates.DONE);
		pipeline.setCurrentStep(first);

		String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(pipeline);
		System.out.println("json = " + json);

		ProtectionContext changedContext = objectMapper.readValue(json, ProtectionContext.class);

	}

}
